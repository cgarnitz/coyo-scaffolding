(function () {
  'use strict';

  var TimelineItem = require('./timeline-item.page');

  function PersonalTimeline() {
    var api = this;

    api.form = {
      container: element(by.css('.timeline-form-inline')),
      messageField: element(by.css('.timeline-form-inline textarea')),
      attachmentTrigger: element(by.css('.timeline-form-attachment-trigger')),
      submitBtn: element(by.css('.timeline-form-inline button'))
    };

    api.stream = element(by.css('.timeline-stream'));

    api.items = TimelineItem.getAllElements(api.stream);

    api.deleteTimelineItemUsingContextMenu = function (timelineItem) {
      var contextMenu = timelineItem.$('.timeline-item-footer .context-menu');
      contextMenu.click();
      var deleteButton = contextMenu.$('.delete-post-button');
      deleteButton.click();
    };

    api.findCommentsForTimelineItem = function (timelineItem) {
      return timelineItem.$$('.comments .comment');
    };

    api.findSetEditModeButtonForComment = function (comment) {
      return comment.$('[ng-click="$ctrl.setEditMode()"]');
    };

    api.findEditModeCommentInputForComment = function (comment) {
      return comment.$('.comment-body-edit .comment-form-textarea');
    };

    api.findNewCommentInputForTimelineItem = function (timelineItem) {
      return timelineItem.$('.comments-form.panel-footer .comments-form-inner textarea');
    };

    api.findCommentSubmitButtonForTimelineItem = function (timelineItem) {
      return timelineItem.element(by.css('button[type=submit]'));
    };

    api.toggleCommentContextMenu = function (comment) {
      comment.$('.context-menu-toggle').click();
    };

    api.findCommentMessageElement = function (comment) {
      return comment.$('.comment-message');
    };

    api.createTimelineItemWithComment = function () {
      var message = 'Some timeline item ' + Math.floor(Math.random() * 1000000);
      api.form.messageField.sendKeys(message);
      api.form.submitBtn.click();

      var timelineItem = api.items.first();
      var commentMessageInput = api.findNewCommentInputForTimelineItem(timelineItem);
      var commentSubmitButton = api.findCommentSubmitButtonForTimelineItem(timelineItem);
      commentMessageInput.sendKeys('Some comment.');
      commentSubmitButton.click();

      return timelineItem;
    };

    api.findEditCancelButtonForComment = function (comment) {
      return comment.$('.comment-form-actions .cancel-editing-comment');
    };

    api.findEditSaveButtonForComment = function (comment) {
      return comment.$('.comment-form-actions .cancel-editing-comment + button');
    };
  }

  module.exports = PersonalTimeline;

})();
