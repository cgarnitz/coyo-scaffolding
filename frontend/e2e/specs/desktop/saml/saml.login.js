(function () {
  'use strict';

  var testhelper = require('../../../testhelper');
  var login = require('../../login.page.js');
  var userDirectoryList = require('../admin/user-directories/user-directories-list.page.js');
  var userDirectoryDetails = require('../admin/user-directories/user-directories-details.page.js');
  var ldapDetails = require('../admin/user-directories/ldap-details.page.js');
  var authProviderList = require('../admin/auth-provider/auth-provider-list.page.js');
  var authProviderDetails = require('../admin/auth-provider/auth-provider-details.page.js');
  var samlProviderDetails = require('../admin/auth-provider/saml-details.page.js');
  var userList = require('../admin/user-mgmt/admin.user-list.page.js');
  var userDetails = require('../admin/user-mgmt/admin.user-details.page');
  var settingsGeneral = require('../admin/settings/admin.settings-general.page.js');
  var components = require('../../components.page.js');
  var samlLoginPage = require('./saml-login.page.js');
  var Navigation = require('../navigation.page.js');

  var SAML_AUTH_PROVIDER = 'Test Saml'; // do not change this

  /*
  var fs = require('fs');
  var path = require('path');

  function sendLongText(el, text) {
    var chunkSize = 200;
    for (var i = 0; i < text.length; i += chunkSize) {
      el.sendKeys(text.substr(i, chunkSize));
    }
  }
  */

  describe('saml login', function () {
    var navigation, key/*, privateKey, certificate*/;

    var ldapName;

    beforeAll(function () {
      testhelper.log('Preparing SAML tests - begin');
      navigation = new Navigation();
      key = Math.ceil(Math.random() * 1000000);
      // privateKey = fs.readFileSync(path.join(__dirname, 'private-key.pem'), 'utf-8');
      // certificate = fs.readFileSync(path.join(__dirname, 'certificate.pem'), 'utf-8');

      login.loginDefaultUser();

      // create ldap user directory
      navigation.profileMenu.open();
      navigation.profileMenu.admin.click();
      navigation.admin.userDirectories.click();
      userDirectoryList.createButton.click();
      ldapName = 'LDAP_' + key;
      userDirectoryDetails.name.sendKeys(ldapName);
      $('.ui-select-container').click(); // TODO
      // userDirectoryDetails.type.openDropdown();
      // userDirectoryDetails.type.search('LDAP');
      $('.ui-select-container input[type="search"]').sendKeys('LDAP');
      userDirectoryDetails.type.selectOption('LDAP');

      // LDAP settings
      ldapDetails.connectionTab.heading.click();
      ldapDetails.connectionTab.host.sendKeys('localhost');
      ldapDetails.connectionTab.port.sendKeys('389');
      ldapDetails.connectionTab.baseDn.sendKeys('ou=company,dc=coyoapp,dc=com');
      ldapDetails.connectionTab.username.sendKeys('cn=admin,dc=coyoapp,dc=com');
      ldapDetails.connectionTab.password.sendKeys('ldap');
      ldapDetails.usersTab.heading.click();
      ldapDetails.usersTab.userDn.sendKeys('ou=people');
      ldapDetails.usersTab.userObjectClass.sendKeys('person');
      ldapDetails.usersTab.userId.sendKeys('entryUUID');
      ldapDetails.usersTab.userUsername.sendKeys('mail');
      ldapDetails.usersTab.userFirstName.sendKeys('givenName');
      ldapDetails.usersTab.userLastName.sendKeys('sn');
      ldapDetails.usersTab.userDisplayName.sendKeys('cn');
      ldapDetails.usersTab.userEmail.sendKeys('mail');
      ldapDetails.syncTab.heading.click();
      ldapDetails.syncTab.pageSize.sendKeys('100');
      ldapDetails.syncTab.justInTimeSync.click();

      userDirectoryDetails.active.click();
      userDirectoryDetails.saveButton.click();

      // create saml auth provider
      navigation.admin.authProviders.click();
      // delete Test Saml if it already exists, because any previous saml test failed
      deleteSamlAuthProvider(SAML_AUTH_PROVIDER);
      authProviderList.createButton.click();
      authProviderDetails.name.sendKeys(SAML_AUTH_PROVIDER);
      element(by.model('$ctrl.authenticationProvider.type')).click(); // TODO
      $('.ui-select-container input[type="search"]').sendKeys('SAML');
      authProviderDetails.type.selectOption('SAML');
      authProviderDetails.active.click();
      samlProviderDetails.generalTab.entityId.sendKeys(samlLoginPage.idpBaseUrl.replace('https://', 'http://')
          + '/simplesaml/saml2/idp/metadata.php');
      samlProviderDetails.generalTab.authenticationEndpoint.sendKeys(samlLoginPage.idpBaseUrl
          + '/simplesaml/saml2/idp/SSOService.php');
      samlProviderDetails.generalTab.logoutEndpoint.sendKeys(samlLoginPage.idpBaseUrl
          + '/simplesaml/saml2/idp/SingleLogoutService.php');
      element(by.model('$ctrl.ngModel.properties.logoutMethod')).click(); // TODO
      samlProviderDetails.generalTab.logoutMethod.selectOption('Local');
      samlProviderDetails.generalTab.authenticationExpiryInSeconds.sendKeys('300');
      element(by.model('$ctrl.ngModel.properties.userDirectory')).click(); // TODO
      // $$('.ui-select-container input[type="search"]').get(1).sendKeys('LDAP');
      // samlProviderDetails.userDirectory.selectOption('LDAP');
      $$('.ui-select-container input[type="search"]').get(2).sendKeys(ldapName);
      samlProviderDetails.generalTab.userDirectory.selectOption(ldapName);

      // sign requests, is ignored by local idp
      // samlProviderDetails.signRequestsTab.heading.click();
      // samlProviderDetails.signRequestsTab.signRequests.click();
      // sendLongText(samlProviderDetails.signRequestsTab.signingCertificate, certificate);
      // sendLongText(samlProviderDetails.signRequestsTab.signingPrivateKey, privateKey);

      samlProviderDetails.validateResponseTab.heading.click();
      samlProviderDetails.validateResponseTab.disableTrustCheck.click();
      samlProviderDetails.generalTab.heading.click();
      authProviderDetails.saveButton.click();

      login.logout();
      testhelper.log('Preparing SAML tests - done');
    });

    it('login with saml', function () {
      // go to login page
      login.get();

      // click login with saml
      samlLoginPage.trigger(SAML_AUTH_PROVIDER);

      // login in saml idp
      samlLoginPage.login('mn1', 'gocoyo');

      // verify logged in user
      expect($('.welcome-widget strong').getText()).toBe('Manuel Neuer');
    });

    it('show error message for known SAML user but unknown local user', function () {
      // go to login page
      login.get();

      // click login with saml
      samlLoginPage.trigger(SAML_AUTH_PROVIDER);

      // login in saml idp
      samlLoginPage.login('xyz', 'gocoyo');

      // verify user was not logged in
      var deText = 'Authentifizierung fehlgeschlagen';
      var enText = 'Authentication Failed';
      expect(login.errorMessage.isDisplayed()).toBeTruthy();
      login.errorMessage.getText().then(function (text) {
        var isFailedAuthententication = (text === deText || text === enText);
        expect(isFailedAuthententication).toBeTruthy();
      });
    });

    afterEach(function () {
      samlLoginPage.idpLogout();
    });

    afterAll(function () {
      testhelper.log('Cleaning up SAML tests - begin');
      login.loginDefaultUser();

      enableDeletedUserAnonymization();
      deleteUser('Manuel Neuer');
      disableDeletedUserAnonymization();
      detachUserFromUserDirectory();

      deleteSamlAuthProvider(SAML_AUTH_PROVIDER);
      deleteUserDirectory(ldapName);
      testhelper.log('Cleaning up SAML tests - end');
    });

    function enableDeletedUserAnonymization() {
      settingsGeneral.get();
      settingsGeneral.enableDeletedUserAnonymization.set();
      settingsGeneral.saveButton.click();
    }

    function disableDeletedUserAnonymization() {
      settingsGeneral.get();
      settingsGeneral.enableDeletedUserAnonymization.reset();
      settingsGeneral.saveButton.click();
    }

    function deleteUser(username) {
      userList.get();
      userList.nameFilter.clear();
      userList.nameFilter.sendKeys(username);
      userList.table.rows.count().then(function (count) {
        if (count > 0) {
          userList.table.rows.get(0).options().open();
          userList.table.rows.get(0).options().deleteOption.click();
          components.modals.confirm.deleteButton.click();
        }
      });
    }

    function detachUserFromUserDirectory() {
      userList.get();
      userList.statusFilter.showDeleted();
      userList.nameFilter.clear();
      userList.table.rows.count().then(function (count) {
        if (count > 0) {
          for (var i = 0; i < count; i++) {
            var deletedUser = userList.table.rows.get(i);
            deletedUser.directory.getText().then(function (directory) {
              var dirName = directory.split(/[ \n\r]+/)[0];
              if (dirName === ldapName) {
                var options = deletedUser.options();
                options.open();
                options.editOption.click();
                userDetails.tabAdvanced.click();
                userDetails.userDirectoryClear.click();
                userDetails.saveButton.click();
              }
            });
          }
        }
      });
      userList.statusFilter.showActive();
    }

    function deleteSamlAuthProvider(name) {
      authProviderList.get();
      authProviderList.nameFilter.sendKeys(name);
      authProviderList.table.rows.count().then(function (count) {
        if (count > 0) {
          authProviderList.table.rows.get(0).options().open();
          authProviderList.table.rows.get(0).options().deleteOption.click();
          components.modals.confirm.deleteButton.click();
        }
      });
    }

    function deleteUserDirectory(name) {
      userDirectoryList.get();
      userDirectoryList.nameFilter.sendKeys(name);
      userDirectoryList.table.rows.count().then(function (count) {
        if (count > 0) {
          userDirectoryList.table.rows.get(0).options().open();
          userDirectoryList.table.rows.get(0).options().deleteOption.click();
          components.modals.confirm.deleteButton.click();
        }
      });
    }
  });

})();
