(function () {
  'use strict';

  var admin = require('../admin.page');

  module.exports = {
    get: function () {
      admin.get();
      admin.navigation.multiLanguageLink.click();
    },
    navigationBar: {
      languagesButton: $('#admin-multiLanguage ul.nav li:first-child span.tab-text'),
      translationsButton: $('#admin-multiLanguage ul.nav li:last-child span.tab-text')
    }
  };

})();
