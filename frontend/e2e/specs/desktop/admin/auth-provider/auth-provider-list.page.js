(function () {
  'use strict';

  var repeater = 'row in $ctrl.authenticationProviders track by row.id';

  module.exports = {
    get: function () {
      browser.get('/admin/authentication-providers');
      require('../../../../testhelper.js').disableAnimations();
    },
    nameFilter: element(by.model('$ctrl.searchTerm')),
    directoryTotal: $('counter span.hidden-xs'),
    createButton: $('.fb-actions-inline a[ui-sref="admin.authentication-providers.create"]'),
    table: {
      rows: {
        get: function (index) {
          var row = $$('tr[ng-repeat="' + repeater + '"]').get(index);
          return {
            row: row,
            name: row.$$('td').get(2),
            type: row.$$('td').get(3),
            status: row.$$('td').get(4),
            options: function () {
              var el = row.$$('td').get(5);
              return {
                open: function () {
                  browser.actions().mouseMove(el).perform();
                  el.$('.dropdown-toggle').click();
                },
                editOption: el.$('span[translate="ADMIN.AUTHENTICATION.OPTIONS.EDIT.MENU"]'),
                deleteOption: el.$('span[translate="ADMIN.AUTHENTICATION.OPTIONS.DELETE.MENU"]'),
                activateOption: el.$('span[translate="ADMIN.AUTHENTICATION.OPTIONS.ACTIVATE.MENU"]'),
                deactivateOption: el.$('span[translate="ADMIN.AUTHENTICATION.OPTIONS.DEACTIVATE.MENU"]')
              };
            }
          };
        },
        names: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(0).getText();
          });
        },
        count: function () {
          return element.all(by.repeater('' + repeater + '')).count();
        }
      }}
  };

})();
