(function () {
  'use strict';

  function Pagination() {
    var api = this;

    api.nextPage = function () {
      $('a[ng-click="selectPage(page + 1, $event)"]').click();
    };

    api.paginationList = element.all(by.repeater('page in pages track by $index'));
    api.activePage = function () {
      return $('.coyo-pagination').element(by.className('active'));
    };

  }

  module.exports = Pagination;

})();
