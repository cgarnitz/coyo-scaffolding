/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var gutil = require('gulp-util');
var path = require('path');

/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  bower_components: 'bower_components'
};

/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
  exclude: [/\/bootstrap\.css/],
  directory: 'bower_components',

  // TODO: Automatically populate from COYOs bower.json
  overrides: {
    "bootstrap-sass": {
      "main": [
        "assets/stylesheets/_bootstrap.scss",
        "assets/javascripts/bootstrap.js",
        "assets/fonts/bootstrap/*"
      ]
    },
    "messageformat": {
      "main": [
        "messageformat.js",
        "locale/*.js"
      ]
    },
    "markdown": {
      "main": "lib/markdown.js"
    },
    "angularjs-rails-resource": {
      "main": [
        "angularjs-rails-resource.js",
        "extensions/snapshots.js"
      ]
    },
    "summernote": {
      "main": [
        "dist/summernote.js",
        "dist/summernote.css",
        "dist/font/summernote.eot",
        "dist/font/summernote.ttf",
        "dist/font/summernote.woff"
      ]
    },
    "moment": {
      "main": [
        "moment.js",
        "locale/de.js"
      ]
    },
    "kalendae": {
      "main": [
        "build/kalendae.standalone.js",
        "build/arrows.png",
        "build/close.png"
      ]
    },
    "ng-tags-input": {
      "main": [
        "ng-tags-input.js",
        "ng-tags-input.css",
        "ng-tags-input.bootstrap.css"
      ]
    },
    "numeral": {
      "main": [
        "numeral.js"
      ]
    }
  }
};

/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function(title) {
  'use strict';

  return function(err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
  };
};

/**
 * Additional script files that need to be accessible by libraries for lazy loading
 */
exports.additionalScripts = [
  path.join(exports.paths.bower_components, 'pdf.js-viewer/pdf.worker.js')
];
